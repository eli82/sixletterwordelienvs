﻿using System;
using BL;

namespace UI
{
    class Program
    {
        private static readonly LetterManager mgr = new LetterManager();

        static void Main(string[] args)
        {
            BasicSixLetterWord();
        }
        
        private static void BasicSixLetterWord()
        {
            foreach (var line in mgr.GetLetters())
            {
                string firstPart = line.Line.ToString();
                string secondPart = "";

                switch (firstPart.Length)
                {
                    case 1:
                        secondPart = mgr.GetOtherWord(1);
                        break;
                    case 2:
                        secondPart = mgr.GetOtherWord(2);
                        break;
                    case 3:
                        secondPart = mgr.GetOtherWord(3, firstPart);
                        break;
                    case 4:
                        secondPart = mgr.GetOtherWord(4);
                        break;
                    case 5:
                        secondPart = mgr.GetOtherWord(5);
                        break;
                    case 6:
                        secondPart = "";
                        break;
                    default:
                        break;
                }

                Console.WriteLine(firstPart + secondPart);
            }
        }
    }
}
