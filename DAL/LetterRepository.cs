﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using Domain;

namespace DAL
{
    public class LetterRepository
    {
        private List<Letter> letters;
        private string fileName = "input.txt";

        public LetterRepository()
        {
            Seed();
        }

        private void Seed()
        {
            letters = new List<Letter>();
            var exePath = Path.GetDirectoryName(System.AppDomain.CurrentDomain.BaseDirectory);
            var path = Path.Combine(Directory.GetParent(Directory.GetParent(Directory.GetParent(exePath).FullName).FullName).FullName, @"Data\", fileName); 
            string[] lines = System.IO.File.ReadAllLines(path);
            
            foreach (string line in lines)
            {
                Letter letter = MakeLetter(line);
                letters.Add(letter);
            }  
        }

        private Letter MakeLetter(string line)
        {
            Letter letter = new Letter()
            {
                Line = line
            };

            return letter;
        }

        public IEnumerable<Letter> ReadLetters()
        {
            return letters;
        }
    }
}
