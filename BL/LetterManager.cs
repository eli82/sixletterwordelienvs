﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain;
using DAL;
using System.Linq;

namespace BL
{
    public class LetterManager
    {
        private readonly LetterRepository repo;

        public LetterManager()
        {
            repo = new LetterRepository();
        }

        public IEnumerable<Letter> GetLetters()
        {
            return repo.ReadLetters();
        }

        public IEnumerable<Letter> GetSpecificLetters(int count, string letter ="")
        {
            var allLetters = repo.ReadLetters();
            
            return allLetters.Where(l => l.Line.Length == count);
        }

        public string GetOtherWord(int count, string letter = "")
        {
            //TODO: check letter (secondPart) is not the same as the firstPart
            var random = new Random();

            int search = 6 - count;

            var specificLetters = GetSpecificLetters(search);
            var rand = random.Next(0, specificLetters.ToArray().Count());

            if (rand == 0)
            {
                return "";
            }
            else
            {
                return specificLetters.ElementAt(rand).Line.ToString();
            }
        }
    }
}
